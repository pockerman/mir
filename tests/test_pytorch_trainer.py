"""unit tests for testing PyTorchTrainer

"""
import unittest
import pytest

import torch.nn as nn
from torch.optim.adam import Adam

from src.trainers.pytorch_trainer import PyTorchTrainer
from src.trainers.pytorch_trainer import PyTorchTrainerConfig


class TestModel(nn.Module):
    def __init__(self):
        super(TestModel, self).__init__()
        self.linear = nn.Linear(28*28, 512)


class TestLoss(object):
    pass


class TestPyTorchTrainer(unittest.TestCase):

    def setUp(self) -> None:
        self.trainer_config = PyTorchTrainerConfig()
        self.trainer_config.model = TestModel()
        self.trainer_config.optimizer = Adam(self.trainer_config.model.parameters())
        self.trainer_config.loss_func = TestLoss()

    def test_ctor_succeeds(self):
        trainer = PyTorchTrainer(config=self.trainer_config)

    def test_ctor_fails_config_is_None(self):

        with pytest.raises(ValueError) as e:
            trainer = PyTorchTrainer(config=None)

            self.assertEqual(str(e), "Configuration object is None")

    def test_ctor_fails_model_is_None(self):

        with pytest.raises(ValueError) as e:
            trainer_config = PyTorchTrainerConfig()
            trainer = PyTorchTrainer(config=trainer_config)

            self.assertEqual(str(e), "Model is not None")

    def test_ctor_fails_optimizer_is_None(self):

        with pytest.raises(ValueError) as e:
            trainer_config = PyTorchTrainerConfig(model=TestModel())
            trainer = PyTorchTrainer(config=trainer_config)

            self.assertEqual(str(e), "Optimizer is None")

    def test_ctor_fails_loss_func_is_None(self):

        with pytest.raises(ValueError) as e:
            trainer_config = PyTorchTrainerConfig()
            trainer_config.model = TestModel()
            trainer_config.optimizer = Adam(trainer_config.model.parameters())
            trainer = PyTorchTrainer(config=trainer_config)

            self.assertEqual(str(e), "Loss function is None")

    def test_ctor_fails_invalid_number_of_epochs(self):

        with pytest.raises(ValueError) as e:
            trainer_config = PyTorchTrainerConfig()
            trainer_config.model = TestModel()
            trainer_config.optimizer = Adam(trainer_config.model.parameters())
            trainer_config.loss_func = TestLoss()
            trainer_config.n_epochs = 0
            trainer = PyTorchTrainer(config=trainer_config)

            self.assertEqual(str(e), "Invalid number of epochs. n_epochs should be >=1")

    def test_fit_fails_train_data_None(self):
        trainer = PyTorchTrainer(config=self.trainer_config)

        with pytest.raises(ValueError) as e:
            trainer.fit(train_data=None)

            self.assertEqual(str(e), "train_data is None")



if __name__ == '__main__':
    unittest.main()