from src.loss_functions.distance_functions import load_distance_functions

DISTANCE_METRIC_MAP = load_distance_functions()
