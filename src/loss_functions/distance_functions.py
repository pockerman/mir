"""module distance_functions. Provides various
distance metrics

"""

from typing import TypeVar

from src.loss_functions.distance_type import DistanceType

Tensor = TypeVar('Tensor')


def load_distance_functions() -> dict:
    """Loads the distance metrics available

    :return: A dictionary that holds the implementation
    of various distance metrics
    """
    distance_metric_map = {DistanceType.L2_DISTANCE: l2_dist}
    return distance_metric_map

def l2_dist(x: Tensor, y: Tensor) -> float:

    return x + y


