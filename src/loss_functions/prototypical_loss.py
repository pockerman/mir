"""module prototypical_loss. Provides implementation
of the calculation of the loss function for PrototypicalNet model
The original implementation comes from: https://github.com/orobix/Prototypical-Networks-for-Few-shot-Learning-PyTorch
"""

import torch
from torch.nn import functional as F
from dataclasses import dataclass

from src.loss_functions.distance_type import DistanceType
from src.loss_functions import DISTANCE_METRIC_MAP


@dataclass(init=True, repr=True)
class PrototypicalLossConfig:
    distance_type: DistanceType = None
    log_softmax_dim: int = 0


class PrototypicalLoss(object):
    """Wrapper class that implements the prototypical loss
    for PrototypicalNet

    """
    def __init__(self, config: PrototypicalLossConfig):

        if config.distance_type is None:
            raise ValueError("distance type is None")

        # raise if invalid distance type
        if config.distance_type == DistanceType.INVALID:
            raise ValueError("distance type is INVALID")

        self.config = config

    def loss(self, input: torch.Tensor, target: torch.Tensor, n_support):
        """Compute the barycentres by averaging the features of n_support
        samples for each class in target, computes then the distances from each
        samples' features to each one of the barycentres, computes the
        log_probability for each n_query samples for each one of the current
        classes, of appartaining to a class c, loss and accuracy are then computed
        and returned

        :param input: The model output for a batch of samples
        :param target: Ground truth for the above batch of samples
        :param n_support: Number of samples to keep in account when computing
        barycentres, for each one of the current classes
        :return:
        """

        # get a copy of the tensors from the
        # GPU
        target_cpu = target.to('cpu')
        input_cpu = input.to('cpu')

        def supp_idxs(c):
            # FIXME when torch will support where as np
            return target_cpu.eq(c).nonzero()[:n_support].squeeze(1)

        # FIXME when torch.unique will be available on cuda too
        classes = torch.unique(target_cpu)
        n_classes = len(classes)

        # FIXME when torch will support where as np
        # assuming n_query, n_target constants
        n_query = target_cpu.eq(classes[0].item()).sum().item() - n_support

        support_idxs = list(map(supp_idxs, classes))

        prototypes = torch.stack([input_cpu[idx_list].mean(0) for idx_list in support_idxs])

        # FIXME when torch will support where as np
        query_idxs = torch.stack(list(map(lambda c: target_cpu.eq(c).nonzero()[n_support:], classes))).view(-1)

        query_samples = input.to('cpu')[query_idxs]
        dists = DISTANCE_METRIC_MAP[self.config.distance_type](query_samples, prototypes)

        # While mathematically equivalent to log(softmax(x)), doing
        # these two operations separately is slower and numerically unstable.
        # This function uses an alternative formulation to compute
        # the output and gradient correctly.
        log_p_y = F.log_softmax(-dists, dim=self.config.log_softmax_dim).view(n_classes, n_query, -1)

        target_inds = torch.arange(0, n_classes)
        target_inds = target_inds.view(n_classes, 1, 1)
        target_inds = target_inds.expand(n_classes, n_query, 1).long()

        loss_val = -log_p_y.gather(2, target_inds).squeeze().view(-1).mean()
        _, y_hat = log_p_y.max(2)
        acc_val = y_hat.eq(target_inds.squeeze(2)).float().mean()

        return loss_val, acc_val