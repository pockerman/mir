"""module pytorch_trainer. Specifies a trainer
for pytorch models. A PyTorchTrainer wraps three main
elements

- A model to train
- A loss model
- An optimizer
- A train & validate data set

"""

from dataclasses import dataclass
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

from typing import TypeVar

LossFunction = TypeVar('LossFunction')


@dataclass(init=True, repr=True)
class PyTorchTrainerConfig(object):
    optimizer: optim.Optimizer = None
    model: nn.Module = None
    loss_func: LossFunction = None
    n_epochs: int = 1


def _validate_config(config: PyTorchTrainerConfig) -> None:
    """Validate the configuration

   Parameters
   ----------
   config: The configuration object to validate

   Returns
   -------
    None
   """

    if config is None:
        raise ValueError("Configuration object is None")

    if config.model is None:
        raise ValueError("Model is not None")

    if config.optimizer is None:
        raise ValueError("Optimizer is None")

    if config.loss_func is None:
        raise ValueError("Loss function is None")

    if config.n_epochs <= 0:
        raise ValueError("Invalid number of epochs. n_epochs should be >=1")


class PyTorchTrainer(object):

    def __init__(self, config):

        _validate_config(config=config)
        self.config: PyTorchTrainerConfig = config

    def fit(self, train_data: DataLoader, validate_data: DataLoader = None) -> None:
        """Fit the model on the given data

        Parameters
        ----------
        train_data
        validate_data:
        Returns
        -------

        """

        if train_data is None:
            raise ValueError("train_data is None")
