"""module pytorch_prototypical_net. Implementation
of prototypical network with pytorch

"""
import torch
import torch.nn as nn

from src.pytorch_util_modules.flatten_module import Flatten

def build_default_encoder(x_dim: int, hid_dim, z_dim,
                          kernel_size: int = 3, padding: int = 1) -> nn.Sequential:
    """Builds the default encoder for the prototypical network

    :param x_dim:
    :param hid_dim:
    :param z_dim:
    :param kernel_size:
    :param padding:
    :return:
    """

    def conv_block(in_channels, out_channels, kernel_size, padding):
        return nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size, padding=padding),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(),
            nn.MaxPool2d(2)
        )

    encoder = nn.Sequential(
        conv_block(x_dim[0], hid_dim, kernel_size=kernel_size, padding=padding),
        conv_block(hid_dim, hid_dim, kernel_size=kernel_size, padding=padding),
        conv_block(hid_dim, hid_dim, kernel_size=kernel_size, padding=padding),
        conv_block(hid_dim, z_dim, kernel_size=kernel_size, padding=padding),
        Flatten()
    )

    return encoder


class PrototypicalNet(nn.Module):
    """class PrototypicalNet provides an implementation
    of prototypical network.
    https://github.com/jakesnell/prototypical-networks/blob/c9bb4d258267c11cb6e23f0a19242d24ca98ad8a/protonets/models/few_shot.py#L18

    """

    def __init__(self, encoder: nn.Sequential):
        """

        :param encoder:
        """
        super(PrototypicalNet, self).__init__()
        self.encoder = encoder

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Pass the data from the model

        Parameters
        ----------
        x: the data to be forwarded by the model

        Returns
        -------

        An instance fo torch.Tensor
        """
        return self.encoder(x)
